# Whiteboard

A simple collaborative whiteboard. Hosted on https://whiteboard.giger.dev.

The app is implemented using the firebase realtime database as a backend.

### Setup

Install dependencies

```
npm install
```

Install firebase CLI

```
npm install -g firebase-tools
```

### Run
Run locally:
```
firebase emulators:start
npm start
```

### Deploy
Deploy to firebase:
```
npm run clean
npm run build
firebase deploy
```

## TODO
- Auto resize stage
- Peer2Peer data exchange
- Clean up old whiteboards
- Share cursor
- Post-It notes
- Delete items on mobile
- Undo, Redo